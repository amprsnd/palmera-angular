import { Component, OnInit} from '@angular/core';
import { Response} from '@angular/http';
import { HttpService} from './http.service';

import {Station, InfoBlocks, Social} from './models';

import { HeaderComponent } from './partials/header.component';
import { FooterComponent } from './partials/footer.component';
import { StationsComponent } from './stations/stations.component';

import { OneStationComponent } from './one-station/one-station.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [HttpService]
})
export class AppComponent implements OnInit {

  stations: Station[]=[];
  socials: Social[]=[];
  info_blocks: InfoBlocks[]=[];

  constructor(private httpService: HttpService){}

  ngOnInit(){

    this.httpService.getStations().subscribe((data: Response) => this.stations=data.json());
    this.httpService.getSocials().subscribe((data: Response) => this.socials=data.json());
    this.httpService.getInfos().subscribe((data: Response) => this.info_blocks=data.json());

  }
}
