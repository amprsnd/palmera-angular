import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class HttpService{

  constructor(private http: Http){ }

  getStations(){
    return this.http.get('/assets/data/stations.json');
  }

  getSocials(){
    return this.http.get('/assets/data/social.json');
  }

  getInfos(){
    return this.http.get('/assets/data/info.json');
  }

}
