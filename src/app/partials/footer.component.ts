import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-footer',
  template: `
    <footer>
      <div class="links">
        <a *ngFor="let info_block of info_blocks" [routerLink]="['info', info_block.name]">{{info_block.title}}</a>
      </div>
      <div class="social">
        <a *ngFor="let social of socials" href="{{social.link}}" target="_blank" class="{{social.prefix}}"></a>
      </div>
    </footer>
  `
})
export class FooterComponent {

  @Input() info_blocks: any;
  @Input() socials: any;

  onOpenInfo(e) {

    console.log('open modal vvv');

    e.preventDefault();

  }

  onCloseInfo() {

    console.log('close modal xxx');

  }

}
