import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  template: `
    <header>
      <div class="logo">
        <img src="assets/images/logo.svg" alt="Palmera Blanca" class="dark">
        <img src="assets/images/logo-light.svg" alt="Palmera Blanca" class="light invisible">
      </div>
      <div class="slogan">Listen, feel, enjoy!</div>
    </header>
  `
})
export class HeaderComponent {}
