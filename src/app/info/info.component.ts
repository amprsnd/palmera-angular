import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent {

  private station: string;
  private subscription: Subscription;
  constructor(private activateRoute: ActivatedRoute){

    this.subscription = activateRoute.params.subscribe(params=>this.station=params['slug']);

  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
