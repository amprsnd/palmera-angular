export class Station {
  id: number;
  slug: string;
  title: string;
  stream_url: string;
  pretty_url: string;
}

export class InfoBlocks {
  name: string;
  title: string;
  content: string;
}

export class Social {
  name: string;
  prefix: string;
  link: string;
}
