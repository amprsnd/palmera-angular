import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-stations',
  templateUrl: './stations.component.html',
  styleUrls: ['./stations.component.css'],
  providers: []
})
export class StationsComponent implements OnInit {

  @Input() stations: any;

  constructor(
    private router: Router
  ) {}

  ngOnInit() {
  }

  onStartStream(e) {
    console.log('start stream! >>>>>');

    e.stopPropagation();
  }

  onOpenStream(slug) {

    this.router.navigate(['/station', slug]);

    console.log('open stream! ======')
  }

}
