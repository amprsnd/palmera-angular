import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {Routes, RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './partials/header.component';
import { FooterComponent } from './partials/footer.component';
import { EmptyComponent }  from './partials/empty.component';
import { StationsComponent } from './stations/stations.component';
import { OneStationComponent } from './one-station/one-station.component';
import { InfoComponent } from './info/info.component';


// определение маршрутов
const appRoutes: Routes =[
  { path: '', component: EmptyComponent },
  { path: 'station/:slug', component: OneStationComponent },
  { path: 'info/:slug', component: InfoComponent },
  { path: '**', component: EmptyComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    StationsComponent,
    OneStationComponent,
    InfoComponent,
    EmptyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
