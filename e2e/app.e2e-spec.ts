import { PalmeraBlancaPage } from './app.po';

describe('palmera-blanca App', () => {
  let page: PalmeraBlancaPage;

  beforeEach(() => {
    page = new PalmeraBlancaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
